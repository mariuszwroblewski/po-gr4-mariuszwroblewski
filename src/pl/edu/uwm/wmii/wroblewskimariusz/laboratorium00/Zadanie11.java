package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium00;

public class Zadanie11 {
    public static void main(String[] args) {
        System.out.println("Istnieli albo nie istnieli.");
        System.out.println("Na wyspie albo nie na wyspie.");
        System.out.println("Ocean albo nie ocean");
        System.out.println("połknął ich albo nie.\n");
        System.out.println("Czy było komu kogoś słuchać kogo?");
        System.out.println("Czy było komu walczyć z kim?");
        System.out.println("Działo się wszystko albo nic");
        System.out.println("tam albo nie tam.\n");
        System.out.println("Miast siedem stało.");
        System.out.println("Czy na pewno?");
        System.out.println("Stać wiecznie chciało.");
        System.out.println("Gdzie dowody?\n");
        System.out.println("Nie wymyślili prochu, nie.\n");
        System.out.println("Proch wymyślili, tak.");
        System.out.println("Przypuszczalni. Wątpliwi.\n");
        System.out.println("Nie wyjęci z powietrza,");
        System.out.println("z ognia, z wody, z ziemi.\n");
        System.out.println("Nie zawarci w kamieniu");
        System.out.println("ani w kropli deszczu.\n");
        System.out.println("Nie mogący na serio");
        System.out.println("pozować do przestróg.\n");
        System.out.println("Meteor spadł.");
        System.out.println("To nie meteor.");
        System.out.println("Wulkan wybuchnął.");
        System.out.println("To nie wulkan.");
        System.out.println("Ktoś wołał coś.");
        System.out.println("Niczego nikt.\n");
        System.out.println("Na tej plus minus Atlantydzie.");

    }
}
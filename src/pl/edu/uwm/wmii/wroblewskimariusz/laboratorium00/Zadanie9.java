package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium00;

public class Zadanie9 {
    public static void main(String[] args) {
        String name=
                        "        /\\_/\\                                   \n"+
                        "      (       )               /^^^^^^^^\\              \n"+
                        "      (=O | O=)             <   Hello   \\                \n"+
                        "      (  |||  )            <    Junior   |            \n"+
                        "        |   |               <   Coder!  /                   \n"+
                        "        |   |                 \\________/                   \n"+
                        "       (_____)                                              \n";

        System.out.println(name);
    }
}

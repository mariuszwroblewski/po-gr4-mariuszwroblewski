package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium10;

import pl.imiajd.wroblewski10.*;

import java.util.ArrayList;

public class TestStudent {
    public static void main(String[] args){
        ArrayList<Student> grupa=new ArrayList<>(5);
        grupa.add(new Student());
        grupa.add(new Student());
        grupa.add(new Student());
        grupa.add(new Student());
        grupa.add(new Student());
        grupa.get(0).setNazwisko("Jan");
        grupa.get(0).setDataUrodzenia(2000,12,23);
        grupa.get(0).setSredniaOcen(2.22);
        grupa.get(1).setNazwisko("Jan");
        grupa.get(1).setDataUrodzenia(2000,12,23);
        grupa.get(1).setSredniaOcen(2.20);
        grupa.get(2).setNazwisko("Nowak");
        grupa.get(2).setDataUrodzenia(1899,11,23);
        grupa.get(2).setSredniaOcen(5.55);
        grupa.get(3).setNazwisko("Kowalki");
        grupa.get(3).setDataUrodzenia(1899,11,23);
        grupa.get(3).setSredniaOcen(2.22);
        grupa.get(4).setNazwisko("Wróblewski");
        grupa.get(4).setDataUrodzenia(1999,1,31);
        grupa.get(4).setSredniaOcen(5.00);
        for (Student os:grupa){
            System.out.println("Nazwisko:"+os.getNazwisko()+" Data urodzenia: "+os.getDataUrodzenia()+" Srednia ocen:"+os.getSredniaOcen());
        }
        grupa.sort(Student::compareTo);
        System.out.println("\nPo sortowaniu\n");
        for (Student os:grupa){
            System.out.println("Nazwisko:"+os.getNazwisko()+" Data urodzenia: "+os.getDataUrodzenia()+" Srednia ocen:"+os.getSredniaOcen());
        }
    }
}

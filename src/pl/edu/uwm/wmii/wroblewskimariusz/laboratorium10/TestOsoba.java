package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium10;

import pl.imiajd.wroblewski10.Osoba;

import java.util.ArrayList;

public class TestOsoba {
    public static void main(String[] args){
        ArrayList<Osoba> grupa=new ArrayList<>(5);
        grupa.add(new Osoba());
        grupa.add(new Osoba());
        grupa.add(new Osoba());
        grupa.add(new Osoba());
        grupa.add(new Osoba());
        grupa.get(0).setNazwisko("Jan");
        grupa.get(0).setDataUrodzenia(2000,12,23);
        grupa.get(1).setNazwisko("Jan");
        grupa.get(1).setDataUrodzenia(2000,12,21);
        grupa.get(2).setNazwisko("Nowak");
        grupa.get(2).setDataUrodzenia(1899,11,23);
        grupa.get(3).setNazwisko("Kowalki");
        grupa.get(3).setDataUrodzenia(1899,11,23);
        grupa.get(4).setNazwisko("Wróblewski");
        grupa.get(4).setDataUrodzenia(1999,1,31);
        for (Osoba os:grupa){
            System.out.println("Nazwisko:"+os.getNazwisko()+" Data urodzenia: "+os.getDataUrodzenia());
        }
        grupa.sort(Osoba::compareTo);
        System.out.println("\nPo sortowaniu\n");
        for (Osoba os:grupa){
            System.out.println("Nazwisko:"+os.getNazwisko()+" Data urodzenia: "+os.getDataUrodzenia());
        }
    }

}
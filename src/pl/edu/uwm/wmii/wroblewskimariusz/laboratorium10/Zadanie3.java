package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium10;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
public class Zadanie3 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner plik=new Scanner(new File(args[0]));
        ArrayList<String> lista=new ArrayList<>();
        while (plik.hasNextLine()) {
            lista.add(plik.nextLine());
        }
        System.out.println(lista);
        Collections.sort(lista);
        System.out.println(lista);
    }
}

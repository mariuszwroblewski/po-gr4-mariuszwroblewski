package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium01;
import java.util.Scanner;

public class Zadanie2_5 {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        System.out.println("Podaj długośc tablicy");
        int num = input.nextInt();

        float[] array = new float[num];

        System.out.println("Podaj kolejne " + num + " wartosci do wczytania");

        for (int i = 0 ; i < array.length; i++ ) {
            array[i] = input.nextFloat();
        }

        int answ=0;
        for(int i=0;i<array.length;i++){
            if(i+1<array.length){
                if(array[i]>0&&array[i+1]>0){
                    answ+=1;
                }
            }
        }
        System.out.println("Wynik");
        System.out.println(answ);
        input.close();

    }

}

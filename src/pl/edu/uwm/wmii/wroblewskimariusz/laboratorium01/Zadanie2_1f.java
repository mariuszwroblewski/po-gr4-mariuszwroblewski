package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium01;
import java.util.Scanner;

public class Zadanie2_1f {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        System.out.println("Podaj długośc tablicy");
        int num = input.nextInt();

        int[] array = new int[num];

        System.out.println("Podaj kolejne " + num + " wartosci do wczytania");

        for (int i = 0 ; i < array.length; i++ ) {
            array[i] = input.nextInt();
        }

        System.out.println("Wynik:");

        int answ=0;
        for(int i=0;i<array.length;i++){
            if((i+1)%2!=0&&array[i]%2==0)
                answ+=1;
        }
        System.out.println(answ);
        input.close();

    }

}

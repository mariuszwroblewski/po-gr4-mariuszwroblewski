package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium01;
import java.util.Scanner;
import java.lang.Math;

public class Zadanie2_1e {
    private static int silnia(int i) {
        if (i < 1)
            return 1;
        else
            return i * silnia(i - 1);
    }
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        System.out.println("Podaj długośc tablicy");
        int num = input.nextInt();

        int[] array = new int[num];

        System.out.println("Podaj kolejne " + num + " wartosci do wczytania");

        for (int i = 0 ; i < array.length; i++ ) {
            array[i] = input.nextInt();
        }

        System.out.println("Wynik:");

        int answ=0;
        for(int i=1;i<array.length;i++){
            if(array[i]>Math.pow(2,array[i])&&array[i]<(silnia(array[i]))) {
                answ += 1;
            }

        }
        System.out.println(answ);
        input.close();

    }

}

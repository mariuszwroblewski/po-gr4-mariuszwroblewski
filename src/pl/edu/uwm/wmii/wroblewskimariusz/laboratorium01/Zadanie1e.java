package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium01;
import java.util.Scanner;
import java.lang.Math;

public class Zadanie1e {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.println("Podaj długośc tablicy");
        int num = input.nextInt();

        int[] array = new int[num];

        System.out.println("Podaj kolejne " + num + " wartosci do wczytania");

        for (int i = 0 ; i < array.length; i++ ) {
            array[i] = input.nextInt();
        }

        System.out.println("Wynik");
        int answ=1;
        for(int i=0;i<array.length;i++){
            answ*=Math.abs(array[i]);
        }
        System.out.println(answ);
        input.close();

    }

}

package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium01;
import java.util.Scanner;

public class Zadanie1i {
    private static int silnia(int i) {
        if (i < 1)
            return 1;
        else
            return i * silnia(i - 1);
    }
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        System.out.println("Podaj długośc tablicy");
        int num = input.nextInt();

        int[] array = new int[num];

        System.out.println("Podaj kolejne " + num + " wartosci do wczytania");

        for (int i = 0 ; i < array.length; i++ ) {
            array[i] = input.nextInt();
        }

        System.out.println("Wynik:");
        int answ=0;
        int n=1;
        for(int i=0;i<array.length;i++){
            if(i%2==0){
                answ+=(array[i]*-1)/silnia(n);
                n+=1;
            }
            else{
                answ+=(array[i])/silnia(n);
                n+=1;
            }
        }
        System.out.println(answ);
        input.close();

    }

}

package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium01;
import java.util.Scanner;

public class Zadanie2_2 {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        System.out.println("Podaj długośc tablicy");
        int num = input.nextInt();

        float[] array = new float[num];

        System.out.println("Podaj kolejne " + num + " wartosci do wczytania");

        for (int i = 0 ; i < array.length; i++ ) {
            array[i] = input.nextFloat();
        }

        System.out.println("Wynik:");

        float answ=0;
        for(int i=0;i<array.length;i++){
            if(array[i]>0)
                answ+=array[i]*2;
        }
        System.out.println(answ);
        input.close();

    }

}

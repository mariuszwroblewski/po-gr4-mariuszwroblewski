package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium01;
import java.util.Scanner;

public class Zadanie2_4 {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        System.out.println("Podaj długośc tablicy");
        int num = input.nextInt();

        float[] array = new float[num];

        System.out.println("Podaj kolejne " + num + " wartosci do wczytania");

        for (int i = 0 ; i < array.length; i++ ) {
            array[i] = input.nextFloat();
        }

        System.out.println("Wynik:");

        float max=array[0];
        float min=array[0];
        for(int i=0;i<array.length;i++){
            if(array[i]>max){
                max=array[i];
            }
            if(array[i]<min){
                min=array[i];
            }
        }
        System.out.println("Najmniejsza wartość:");
        System.out.println(min);
        System.out.println("Największa wartość:");
        System.out.println(max);
        input.close();

    }

}

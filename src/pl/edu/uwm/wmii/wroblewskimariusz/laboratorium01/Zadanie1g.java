package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium01;
import java.util.Scanner;

public class Zadanie1g {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.println("Podaj długośc tablicy");
        int num = input.nextInt();

        int[] array = new int[num];

        System.out.println("Podaj kolejne " + num + " wartosci do wczytania");

        for (int i = 0 ; i < array.length; i++ ) {
            array[i] = input.nextInt();
        }

        System.out.println("Suma wartości tablicy:");
        int answ1=0;
        for(int i=0;i<array.length;i++){
            answ1+=array[i];
        }
        System.out.println(answ1);
        System.out.println("Iloczyn wartości tablicy:");
        int answ2=1;
        for(int i=0;i<array.length;i++){
            answ2*=array[i];
        }
        System.out.println(answ2);
        input.close();

    }

}

package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium01;
import java.util.Scanner;

public class Zadanie2_3 {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        System.out.println("Podaj długośc tablicy");
        int num = input.nextInt();

        float[] array = new float[num];

        System.out.println("Podaj kolejne " + num + " wartosci do wczytania");

        for (int i = 0 ; i < array.length; i++ ) {
            array[i] = input.nextFloat();
        }

        System.out.println("Wynik:");

        int dod=0;
        int ujem=0;
        int zera=0;
        for(int i=0;i<array.length;i++){
            if(array[i]>0){
                dod+=1;
            }
            if(array[i]<0){
                ujem+=1;
            }
            if(array[i]==0){
                zera+=1;
            }
        }
        System.out.println("Ilosć liczb dodatnich:");
        System.out.println(dod);
        System.out.println("Ilosć liczb ujemnych:");
        System.out.println(ujem);
        System.out.println("Ilosć zer:");
        System.out.println(zera);
        input.close();

    }

}

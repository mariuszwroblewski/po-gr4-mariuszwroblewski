package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium08;
import pl.imiajd.wroblewski08.*;
import java.time.LocalDate;

public class TestOsoba {
    public static void main(String[] args) {
        Osoba[] ludzie = new Osoba[2];
        String[] imiona={"Imie1","Imie2"};
        LocalDate data=LocalDate.now();
        LocalDate zatrudn=LocalDate.now().minusYears(20);
        ludzie[0] = new Pracownik("Kowalski", imiona,data,zatrudn,true,6000);
        ludzie[1] = new Student("Nowak",imiona,data,false,"Informatyka");
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");
        for (Osoba p : ludzie) {
            System.out.println("Nazwisko:"+p.getNazwisko() +" "+ p.getOpis());
        }
    }
}
package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium08;
import pl.imiajd.wroblewski08.*;


import java.util.ArrayList;
public class TestInstrument {

    public static void main(String[] args){
        ArrayList<Instrument> orkiestra=new ArrayList<>();
        orkiestra.add(new Flet());
        orkiestra.add(new Skrzypce());
        orkiestra.add(new Flet());
        orkiestra.add(new Fortepian());
        orkiestra.add(new Skrzypce());
        for(Instrument a:orkiestra){
            a.dzwiek();
        }
        for(Instrument a:orkiestra){
            System.out.println("Instrument: "+a.getClass().getSimpleName());
        }
    }
}

package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium07;

import pl.imiajd.wroblewski.*;

public class TestOsoba {
    public static void main(String[] args){
        Student s1=new Student("Kowalki",1999,"Informatyka");
        Nauczyciel n1=new Nauczyciel("Nowak",1956,4500);
        System.out.println(s1.toString());
        System.out.println(n1.toString());
        Osoba osoba=new Nauczyciel("nazw",1322,1232);
        System.out.println(osoba.toString());
        n1.setPensja(199999);
        n1.setRokUrodzenia(123);
        n1.setNazwisko("po_zmianie");
        System.out.println(n1.toString());
        System.out.println("---------------------------------\n");
        System.out.println(n1.getNazwisko());
        System.out.println(n1.getRokUrodzenia());
        System.out.println(s1.getKierunek());
    }
}

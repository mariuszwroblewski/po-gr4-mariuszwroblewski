package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium12;

import java.time.LocalDate;
import java.util.*;

public class Zadanie8 {
    public static <E> void print(Iterable<E> lista){
        for (E t : lista){
            System.out.print(t+",");
        }
        System.out.println("\n");
    }
    public static void main(String[] args){
        ArrayList<String> lista1=new ArrayList<>();
        lista1.add("imie1");
        lista1.add("imie2");
        lista1.add("imie3");
        lista1.add("imie4");
        lista1.add("imie5");
        Zadanie8.print(lista1);
        Stack<Integer> stack=new Stack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        Zadanie8.print(stack);
    }
}
package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium12;

import java.util.LinkedList;
import java.util.ListIterator;

public class Zadanie2<T> {
    public static <T> void redukuj(LinkedList<T> pracownicy, int n){
        ListIterator<T> iter=pracownicy.listIterator();
        int i=1;
        while(iter.hasNext()&&(n-1)*i< pracownicy.size()){
            pracownicy.remove((n-1)*i);
            i++;
        }
    }

    public static void main(String[] args){
        LinkedList<Integer> pracownicy=new LinkedList<>();
        ListIterator<Integer> iter=pracownicy.listIterator();
        iter.add(1);
        iter.add(2);
        iter.add(3);
        iter.add(4);
        iter.add(5);
        iter.add(6);
        iter.add(7);
        iter.add(8);
        iter.add(9);
        iter.add(10);
        iter.add(11);
        iter.add(12);
        iter.add(13);
        iter.add(14);
        iter.add(15);
        iter.add(16);
        iter.add(17);
        iter.add(18);
        iter.add(19);
        iter.add(20);
        System.out.println(pracownicy);
        Zadanie2.redukuj(pracownicy,3);
        System.out.println(pracownicy);
    }
}

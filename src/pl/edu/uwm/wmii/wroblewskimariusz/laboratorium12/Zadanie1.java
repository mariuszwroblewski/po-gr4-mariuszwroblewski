package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium12;

import java.util.LinkedList;
import java.util.ListIterator;

public class Zadanie1 {
    public static void redukuj(LinkedList<String> pracownicy, int n){
        ListIterator<String> iter=pracownicy.listIterator();
        int i=1;
        while(iter.hasNext()&&(n-1)*i< pracownicy.size()){
                pracownicy.remove((n-1)*i);
                i++;
        }
        }

    public static void main(String[] args){
        LinkedList<String> pracownicy=new LinkedList<>();
        ListIterator<String> iter=pracownicy.listIterator();
        iter.add("pracownik 1");
        iter.add("pracownik 2");
        iter.add("pracownik 3");
        iter.add("pracownik 4");
        iter.add("pracownik 5");
        iter.add("pracownik 6");
        iter.add("pracownik 7");
        iter.add("pracownik 8");
        iter.add("pracownik 9");
        iter.add("pracownik 10");
        iter.add("pracownik 11");
        iter.add("pracownik 12");
        iter.add("pracownik 13");
        iter.add("pracownik 14");
        iter.add("pracownik 15");
        iter.add("pracownik 16");
        iter.add("pracownik 17");
        iter.add("pracownik 18");
        iter.add("pracownik 19");
        iter.add("pracownik 20");
        System.out.println(pracownicy);
        Zadanie1.redukuj(pracownicy,3);
        System.out.println(pracownicy);
    }
}

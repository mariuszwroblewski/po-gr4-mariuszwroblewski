package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium12;


import java.util.LinkedList;

public class Zadanie3 {

    public static void odwroc(LinkedList<String> list)
    {
        LinkedList<String> revList = new LinkedList<>();
        for (int i = list.size() - 1; i >= 0; i--) {
            revList.add(list.get(i));
        }
        list.clear();
        list.addAll(revList);
        System.out.println(list);
    }
    public static void main(String[] args){
        LinkedList<String> list = new LinkedList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        System.out.println(list);
        Zadanie3.odwroc(list);

    }
}

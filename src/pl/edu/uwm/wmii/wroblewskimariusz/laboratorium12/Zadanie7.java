package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium12;

public class Zadanie7 {

public static void sito(int n) {
    int[] pierwsze = new int[n + 1];
    for (int i = 0; i < n; i++) {
        pierwsze[i] = i;
        }
    for(int i = 2; i * i <= n; i++) {
        if (pierwsze[i] > 0) {
            for (int j = i * 2; j <= n; j += i){
                pierwsze[j] = 0;
            }
        }
    }
    int size = 0;
    for (int i = 2; i <= n; i++) {
        if (pierwsze[i] > 0) {
            size++;
        }
    }
    int[] primes = new int[size];
    int j = 0;
    for (int i = 2; i <= n; i++) {
        if (pierwsze[i] > 0) {
            primes[j] = i;
            j++;
        }
    }
    for(int elem:primes){
        System.out.print(elem+" ");
    }
}
public static void main(String[] args){
    Zadanie7.sito(20);
    }
}

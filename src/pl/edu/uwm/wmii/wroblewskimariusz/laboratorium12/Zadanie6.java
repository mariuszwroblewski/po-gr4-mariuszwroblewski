package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium12;

import java.util.Stack;

public class Zadanie6 {
    public static void podziel(int n){
        Stack<Integer> stos=new Stack<>();
        int dl=String.valueOf(n).length();
        for(int j=0;j<dl;j++) {
            int l1 = n % 10;
            stos.push(l1);
            n=n/10;
        }
        Stack<Integer> wynik=new Stack<>();
        for (int i=0;i<dl;i++){
            wynik.push(stos.pop());
        }
        System.out.println(wynik);
    }
    public static void main(String[] args){
        podziel(2015);
    }
}

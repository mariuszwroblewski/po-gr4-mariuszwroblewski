package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium12;


import java.util.LinkedList;

public class Zadanie4 {

    public static <E> void odwroc(LinkedList<E> list)
    {
        LinkedList<E> revList = new LinkedList<>();
        for (int i = list.size() - 1; i >= 0; i--) {
            revList.add(list.get(i));
        }
        list.clear();
        list.addAll(revList);
        System.out.println(list);
    }
    public static void main(String[] args){
        LinkedList<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(3);
        list.add(12);
        list.add(24);
        list.add(100);
        System.out.println(list);
        Zadanie4.odwroc(list);

    }
}

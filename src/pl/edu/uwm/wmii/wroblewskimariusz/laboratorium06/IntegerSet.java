package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium06;

public class IntegerSet {
    public IntegerSet(){
        this.set=new boolean[100];
    }
    public static void wypisz(IntegerSet set){
        for(int i=0;i<100;i++){
                System.out.println(set.getSet(i));
        }
    }
    public boolean getSet(int i){
        return set[i];
    }
    public static boolean[] union(IntegerSet set1,IntegerSet set2){
        boolean[] sum = new boolean[100];
        for(int i=0;i<100;i++){
            if(set1.set[i]&&set2.set[i]){
                sum[i]=true;
            }
            else{
                sum[i]=false;
            }
        }
        return sum;
    }
    public static boolean[] intersection(IntegerSet set1,IntegerSet set2){
        boolean[] ilocz = new boolean[100];
        for(int i=0;i<100;i++){
            if(set1.set[i]==set2.set[i]){
                ilocz[i]=true;
            }
            else{
                ilocz[i]=false;
            }
        }
        return ilocz;
    }
    public void InsertElement(int a){
        if(a>0&&a<100){
            set[a]=true;
        }
        else {
            set[a]=false;
        }

    }
    public void deleteElement(int a){
        set[a]=false;
    }
    @Override
    public String toString(){
        String wynik="";
        for (int i=0;i<set.length;i++){
            wynik+=i+1+" ";
        }
        return wynik;
    }
    public boolean equals(boolean[] otcherSet){
        for (int i=0;i<100;i++){
            if(!(set[i]==otcherSet[i])){
                return false;
            }
        }
        return true;
    }
    boolean[] set;
}
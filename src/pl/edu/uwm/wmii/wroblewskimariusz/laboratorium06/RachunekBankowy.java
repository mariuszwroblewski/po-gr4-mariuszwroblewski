package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium06;

public class RachunekBankowy {
    public RachunekBankowy(double saldo){

        this.saldo=saldo;
    }
    public void ObliczMiesieczneOdsetki(){
        double odsetki=(saldo*rocznaStopaProcentowa)/12;
        saldo+=odsetki;
    }
    public double getSaldo(){
        return saldo;
    }
    static void setRocznaStopaProcentowa(double a){

        rocznaStopaProcentowa=a;
    }
    static double rocznaStopaProcentowa;
    private double saldo;
}

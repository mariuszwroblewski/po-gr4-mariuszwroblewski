package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium05;
import java.util.Stack;
import java.util.ArrayList;

public class Zadanie4 {
    public static void main(String[] args){
        ArrayList<Integer> list1= new ArrayList<>();
        ArrayList<Integer> list2= new ArrayList<>();
        for(int i=0;i<5;i++){
            list1.add(i,i+1);
        }
        System.out.println(list1);
        list2=reversed(list1);
        System.out.println(list2);
    }
    public static ArrayList<Integer> reversed(ArrayList<Integer> a){
        Stack<Integer> stos=new Stack<>();
        ArrayList<Integer> wynik= new ArrayList<>();
        for(int i=0;i<a.size();i++){
            stos.push(a.get(i));
        }
        for (int j=0;j<a.size();j++){
            wynik.add(stos.pop());
        }
        return wynik;
    }
}

package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium05;
import java.util.ArrayList;

public class Zadanie1 {
    public static void main(String[] args){
        ArrayList<Integer> list1= new ArrayList<>();
        ArrayList<Integer> list2= new ArrayList<>();
        for(int i=0;i<5;i++){
            list1.add(i,i+1);
            list2.add(i,i+11);
        }
        System.out.println(list1);
        System.out.println(list2);
        System.out.println(append(list1,list2));
    }
    public static ArrayList<Integer> append(ArrayList<Integer> a,ArrayList<Integer> b){
        ArrayList<Integer> wynik= new ArrayList<>();
        wynik.addAll(0,a);
        wynik.addAll(a.size(),b);
        return wynik;
    }
}

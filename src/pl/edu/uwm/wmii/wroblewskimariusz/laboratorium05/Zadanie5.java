package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium05;
import java.util.Stack;
import java.util.ArrayList;

public class Zadanie5 {
    public static void main(String[] args){
        ArrayList<Integer> list1= new ArrayList<>();
        for(int i=0;i<5;i++){
            list1.add(i,i+1);
        }
        System.out.println(list1);
        reversed(list1);
        System.out.println(list1);
    }
    public static void reversed(ArrayList<Integer> a){
        Stack<Integer> stos=new Stack<>();
        for(int i=0;i<a.size();i++){
            stos.push(a.get(i));
        }
        for (int j=0;j<a.size();j++){
            a.set(j,stos.pop());
        }
    }
}

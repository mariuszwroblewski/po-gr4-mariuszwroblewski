package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium05;
import java.util.ArrayList;
import java.util.Stack;
import java.util.Collections;
import java.lang.Math;

public class Zadanie2 {
    public static void main(String[] args){
        ArrayList<Integer> list1= new ArrayList<>();
        ArrayList<Integer> list2= new ArrayList<>();
        ArrayList<Integer> list3= new ArrayList<>();
        for(int i=0;i<5;i++){
            list1.add(i,i+1);
            list2.add(i,i+11);
        }
        list2.add(5,20);
        list2.add(6,21);
        list2.add(7,22);
        list2.add(8,23);
        list2.add(9,24);
        System.out.println(list1);
        System.out.println(list2);
        list3=merge(list1,list2);
        Collections.reverse(list3);
        System.out.println(list3);
    }
    public static ArrayList<Integer> merge(ArrayList<Integer> a,ArrayList<Integer> b){
        ArrayList<Integer> wynik=new ArrayList<>(a.size()*b.size());
        int dl1=a.size();
        int dl2=b.size();
        int roznica=Math.abs(dl1-dl2);
        Stack<Integer> stos=new Stack<>();
        if(dl1==dl2){
            for(int i=0;i<dl1;i++){
                stos.push(a.get(i));
                stos.push(b.get(i));
            }
            for(int i=0;i<dl1+dl2;i++){
                wynik.add(i,stos.pop());
            }
        }
        else if(dl1>dl2){
            for(int i=0;i<dl2;i++){
                stos.push(a.get(i));
                stos.push(b.get(i));
            }
            for (int i=dl2;i<dl2+roznica;i++) {
                stos.push(a.get(i));
            }
            for(int i=0;i<dl1+dl2;i++){
                wynik.add(i,stos.pop());
            }
        }
        else{
            for(int i=0;i<dl1;i++){
                stos.push(a.get(i));
                stos.push(b.get(i));
            }
            for (int i=dl1;i<dl1+roznica;i++) {
                stos.push(b.get(i));
            }
            for(int i=0;i<dl1+dl2;i++){
                wynik.add(i,stos.pop());
            }
        }
        return wynik;
        }



    }
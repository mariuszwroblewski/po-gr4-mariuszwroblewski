package pl.edu.uwm.wmii.wroblewskimariusz.kolokwium_2.pl.imiajd.wroblewski;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class Ksiazka implements Cloneable,Comparable<Ksiazka> {
    public Ksiazka(Autor autorr, String tytul, double cena){
        this.autor = new Autor(autorr.getNazwa(), autorr.getEmail(), autorr.getPlec());
        this.tytul=tytul;
        this.cena=cena;
    }
    public int compareTo(Ksiazka o) {
        if(this.autor.compareTo(o.autor) < 0){
            return -1;
        }
        else if(this.autor.compareTo(o.autor) > 0){
            return 1;
        }
        else {
            if(this.tytul.compareTo(o.tytul) < 0){
                return -1;
            }
            else if(this.tytul.compareTo(o.tytul) > 0){
                return 1;
            }
            else {
                if(this.cena<o.cena){
                    return -1;
                }
                else if(this.cena>o.cena){
                    return 1;
                }
                return 0;
           }
        }
    }
    public static void wypiszk(ArrayList<Ksiazka> lista){
        for(Ksiazka a: lista){
            System.out.println(a);
        }
    }
    public String toString() {
        return "Autor[nazwisko= "+autor.getNazwa()+ ", email=: "+autor.getEmail()+", plec= "+autor.getPlec()+", tytuł= "+tytul+", cena= "+cena;
    }
    public static void redukuj(LinkedList<String> books, int n){
        ListIterator<String> iter=books.listIterator();
        int i=1;
        while(iter.hasNext()&&(n-1)*i< books.size()){
            books.remove((n-1)*i);
            i++;
        }
    }
    public Ksiazka clone() throws CloneNotSupportedException {
        Ksiazka cloned= (Ksiazka) super.clone();
        return cloned;
    }
    private final Autor autor;
    private final String tytul;
    private final double cena;
}

package pl.edu.uwm.wmii.wroblewskimariusz.kolokwium_2.pl.imiajd.wroblewski;

import java.util.ArrayList;

public class Autor implements Cloneable, Comparable<Autor>{
    public Autor(String nazwa, String mail, char plec){
        this.plec=plec;
        this.nazwa=nazwa;
        this.email=mail;
    }
    public char getPlec() {
        return plec;
    }
    public String getEmail() {
        return email;
    }
    public String getNazwa() {
        return nazwa;
    }
    public void setPlec(char plec) {
        this.plec = plec;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
    public int compareTo(Autor o) {
        if(this.nazwa.compareTo(o.nazwa) < 0){
            return -1;
        }
        else if(this.nazwa.compareTo(o.nazwa) > 0){
            return 1;
        }
        else {
            if(this.plec<o.plec){
                return -1;
            }
            else if(this.plec>o.plec){
                return 1;
            }
            else {
                return 0;
            }
        }
    }
    public String toString() {
        return "Autor[nazwisko= "+getNazwa()+ ", email=: "+getEmail()+", plec= "+getPlec();
    }
    public static void wypisz(ArrayList<Autor> lista){
        for(Autor a: lista){
            System.out.println(a);
        }
    }

    @Override
    public Autor clone() throws CloneNotSupportedException {
        Autor cloned= (Autor) super.clone();
        return cloned;
    }

    private String nazwa;
    private String email;
    private char plec;
}

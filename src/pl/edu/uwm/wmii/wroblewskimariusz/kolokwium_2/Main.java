package pl.edu.uwm.wmii.wroblewskimariusz.kolokwium_2;

import pl.edu.uwm.wmii.wroblewskimariusz.kolokwium_2.pl.imiajd.wroblewski.Autor;
import pl.edu.uwm.wmii.wroblewskimariusz.kolokwium_2.pl.imiajd.wroblewski.Ksiazka;
import java.util.ArrayList;
import java.util.LinkedList;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        ArrayList<Autor> lista= new ArrayList<>();
        Autor a1=new Autor("Anna","mail@gmail.com", 'm');
        Autor a2=new Autor("Anna","mail@gmail.com",'k');
        Autor a3=new Autor("Dalli" ,"maiiaia@gmail.com",'m');
        Autor a4=new Autor("Mann", "sasaas@gmail.com",'k');
        lista.add(a1);
        lista.add(a2);
        lista.add(a3);
        lista.add(a4);
        Autor.wypisz(lista);
        lista.sort(Autor::compareTo);
        System.out.println();
        Autor.wypisz(lista);
        ArrayList<Ksiazka> ListaKsiazek=new ArrayList<>();
        Ksiazka k1=new Ksiazka(a1,"Hobbit",12.3);
        Ksiazka k2=new Ksiazka(a1,"Hobbit",9.3);
        Ksiazka k3=new Ksiazka(a3,"Hobbita",245.6);
        Ksiazka k4=new Ksiazka(a4,"Hobbita",224.3);
        ListaKsiazek.add(k3);
        ListaKsiazek.add(k4);
        ListaKsiazek.add(k1);
        ListaKsiazek.add(k2);
        System.out.println();
        Ksiazka.wypiszk(ListaKsiazek);
        System.out.println();
        ListaKsiazek.sort(Ksiazka::compareTo);
        Ksiazka.wypiszk(ListaKsiazek);
        System.out.println("\n\nZadanie 2");
        LinkedList<String> listared=new LinkedList<>();
        listared.add(k1.toString());
        listared.add(k2.toString());
        listared.add(k2.toString());
        listared.add(k3.toString());
        listared.add(k4.toString());
        listared.add(k4.toString());
        listared.add(k1.toString());
        listared.add(k3.toString());
        for(String elem:listared){
            System.out.println(elem);
        }
        System.out.println();
        Ksiazka.redukuj(listared,3);
        for(String elem:listared){
            System.out.println(elem);
        }
    }
}

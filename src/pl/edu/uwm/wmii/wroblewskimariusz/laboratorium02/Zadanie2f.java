package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium02;

public class Zadanie2f {
    public static void main(String[] args){
        int n=10;
        int[] tab= new int[n];
        random.generuj(tab,n,-999,1000);
        System.out.println("Funkcja signum:");
        signum(tab);
        for(int elem:tab){
            System.out.println(elem);
        }
    }
    public static void signum(int[] tab) {
        for (int i=0;i< tab.length;i++) {
            if (tab[i] < 0) {
                tab[i] = -1;
            }
            else if (tab[i] > 0) {
                tab[i] = 1;
            }
        }
    }
}

package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium02;

public class Zadanie1d {
    public static void main(String[] args){
        int num = 5;
        int[] array = new int[num];
        array[0]=-3;
        array[1]=2;
        array[2]=123;
        array[3]=-232;
        array[4]=0;
        int ujem_suma=0;
        int dod_suma=0;
        for(int elem:array){
            if(elem<0){
                ujem_suma+=elem;
            }
            else if (elem>0){
                dod_suma+=elem;
            }
        }
        System.out.println("Suma liczb ujemnych w tablicy:");
        System.out.println(ujem_suma);
        System.out.println("Suma liczb dodatnich w tablicy:");
        System.out.println(dod_suma);

    }


}

package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium02;

public class Zadanie2e {
    public static void main(String[] args){
        int n=10;
        int[] tab= new int[n];
        random.generuj(tab,n,-999,1000);
        System.out.println("Długość maksymalnego ciagu dodatnich liczb w tablicy:");
        System.out.println(dlugoscMaksymalnegoCiaguDodatniego(tab));

    }
    public static int dlugoscMaksymalnegoCiaguDodatniego(int[] tab){
        int dl=0;
        int wynik=0;
        for(int elem:tab){
            if(elem>0){
                dl++;
            }
            else if(elem<0){
                if(dl>wynik){
                    wynik=dl;
                }
                dl=0;
            }
        }
        return wynik;
    }
}

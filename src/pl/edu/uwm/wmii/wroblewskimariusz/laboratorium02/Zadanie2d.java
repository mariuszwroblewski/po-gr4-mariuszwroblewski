package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium02;

public class Zadanie2d {
    public static void main(String[] args){
        int n=10;
        int[] tab= new int[n];
        random.generuj(tab,n,-999,1000);
        System.out.println("Suma elementów dodatnich:");
        System.out.println(sumaDodatnich(tab));
        System.out.println("Suma elementów ujemnych:");
        System.out.println(sumaUjemnych(tab));
    }
    public static int sumaDodatnich(int[] tab){
        int suma=0;
        for(int elem:tab){
            if(elem>0){
                suma+=elem;
            }
        }
        return suma;
    }
    public static int sumaUjemnych(int[] tab){
        int suma=0;
        for(int elem:tab){
            if(elem<0){
                suma+=elem;
            }
        }
        return suma;
    }
}

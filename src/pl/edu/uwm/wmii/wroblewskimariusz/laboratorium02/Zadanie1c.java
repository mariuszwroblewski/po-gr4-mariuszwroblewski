package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium02;

public class Zadanie1c {
    public static void main(String[] args){
        int num = 5;
        int[] array = new int[num];
        array[0]=-3;
        array[1]=2;
        array[2]=123;
        array[3]=-232;
        array[4]=123;
        int number=0;
        int max=array[0];
        for(int elem:array){
            if(elem>max){
                max=elem;
            }
        }
        for (int elem:array){
            if(elem==max){
                number++;
            }
        }
        System.out.println("Ilosc wyst. liczby najwiekszej w tablicy:");
        System.out.println(number);

    }


}

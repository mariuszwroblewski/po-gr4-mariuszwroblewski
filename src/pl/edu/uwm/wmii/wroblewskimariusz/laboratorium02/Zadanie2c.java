package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium02;

public class Zadanie2c {
    public static void main(String[] args){
        int n=10;
        int[] tab= new int[n];
        random.generuj(tab,n,-999,1000);
        System.out.println("Ilosc liczb parzystych w tablicy:");
        System.out.println(ileMaksymalnych(tab));

    }
    public static int ileMaksymalnych(int[] tab){
        int number=0;
        int max=tab[0];
        for(int elem:tab){
            if(elem>max){
                max=elem;
            }
        }
        for (int elem:tab){
            if(elem==max){
                number++;
            }
        }

        return number;
    }


}
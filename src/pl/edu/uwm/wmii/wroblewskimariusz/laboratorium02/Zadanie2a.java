package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium02;

public class Zadanie2a {
    public static void main(String[] args){
        int n=10;
        int[] tab= new int[n];
        random.generuj(tab,n,-999,1000);
        System.out.println("Ilosc liczb parzystych w tablicy:");
        System.out.println(ileParzystych(tab));
        System.out.println("Ilosc liczb nieparzystych w tablicy:");
        System.out.println(ileNieparzystych(tab));
    }
    public static int ileParzystych(int[] tab){
        int parz=0;
        for(int element: tab){
            if(element%2==0){
                parz++;
            }

        }
        return parz;
    }
    public static int ileNieparzystych(int[] tab){
        int niep=0;
        for(int element: tab){
            if(!(element%2==0)){
                niep++;
            }

        }
        return niep;
    }

}
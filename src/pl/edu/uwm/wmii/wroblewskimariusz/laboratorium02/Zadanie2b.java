package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium02;

public class Zadanie2b {
    public static void main(String[] args){
        int n=10;
        int[] tab= new int[n];
        random.generuj(tab,n,-999,1000);
        System.out.println("Ilosc liczb dodatnich w tablicy:");
        System.out.println(ileDodatnich(tab));
        System.out.println("Ilosc liczb ujemnych w tablicy:");
        System.out.println(ileUjemnych(tab));
        System.out.println("Ilosc zer w tablicy:");
        System.out.println(ileZerowych(tab));
    }
    public static int ileDodatnich(int[] tab){
        int num=0;
        for(int element: tab){
            if(element>0){ num++;
            }

        }
        return num;
    }
    public static int ileUjemnych(int[] tab){
        int num=0;
        for(int element: tab){
            if(element<0){ num++;
            }

        }
        return num;
    }
    public static int ileZerowych(int[] tab){
        int num=0;
        for(int element: tab){
            if(element==0){ num++;
            }

        }
        return num;
    }

}
package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium02;

public class Zadanie1a {
    public static void main(String[] args){
        int num = 5;
        int[] array = new int[num];
        array[0]=-3;
        array[1]=2;
        array[2]=123;
        array[3]=-232;
        array[4]=0;
        int niep=0;
        int parz=0;
        for(int elem:array){
            if(elem%2==0){
                parz++;
            }
            else{
                niep++;
            }
        }
        System.out.println("Ilosc liczb nieparzystych w tablicy:");
        System.out.println(niep);
        System.out.println("Ilosc liczb parzystych w tablicy:");
        System.out.println(parz);

    }


}

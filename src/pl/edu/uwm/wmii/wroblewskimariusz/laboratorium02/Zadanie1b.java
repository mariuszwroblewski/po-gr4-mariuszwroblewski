package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium02;

public class Zadanie1b {
    public static void main(String[] args){
        int num = 5;
        int[] array = new int[num];
        array[0]=-3;
        array[1]=2;
        array[2]=123;
        array[3]=-232;
        array[4]=0;
        int dod=0;
        int ujem=0;
        int zero=0;
        for(int elem:array){
            if(elem<0){
                ujem++;
            }
            else if (elem>0){
                dod++;
            }
            else{
                zero++;
            }
        }
        System.out.println("Ilosc liczb ujemnych w tablicy:");
        System.out.println(ujem);
        System.out.println("Ilosc liczb dodatnich w tablicy:");
        System.out.println(dod);
        System.out.println("Ilosc zer w tablicy:");
        System.out.println(zero);

    }


}

package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium02;

public class Zadanie1f {
    public static void main(String[] args){
        int num = 5;
        int[] array = new int[num];
        array[0]=-3;
        array[1]=2;
        array[2]=123;
        array[3]=-232;
        array[4]=0;
        for (int i=0;i< array.length;i++) {
            if (array[i] < 0) {
                array[i] = -1;
            }
            else if (array[i] > 0) {
                array[i] = 1;
            }
        }
        for (int elem:array){
            System.out.println(elem);
        }

    }

}

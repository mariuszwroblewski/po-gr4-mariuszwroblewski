package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium02;

public class Zadanie3 {
    public static void main(String[] args){
        int m=3;
        int n=4;
        int k=3;
        int[][] a=new int [m][n];
        random.generuj2(a,m,n,1,10);
        System.out.println("\n");
        int[][] b=new int [n][k];
        random.generuj2(b,n,k,1,10);
        System.out.println("\n");
        int [][] c=multiply(a,b);
        wypisz(c,3,3);

    }
    public static int[][] multiply(int[][] tab1, int[][] tab2) {

        int[][] wynik=new int[tab1.length][tab2[0].length];
        if (tab1.length==tab2[0].length) {
            for (int i=0;i<wynik.length;i++) {
                for (int j=0;j<wynik[0].length;j++) {
                    wynik[i][j]=0;
                    for (int x=0;x<tab2.length;x++) {
                        wynik[i][j]=wynik[i][j]+tab1[i][x]*tab2[x][j];
                    }
                }
            }
        }
        else {
            wynik=null;
        }
        return wynik;
    }
    public static void wypisz(int[][] tab, int m, int n){
        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++ ){
                System.out.print(tab[i][j]+"\t");
            }
            System.out.print("\n");
        }
    }
}

package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium13;

import java.util.TreeMap;

public class Zadanie3 {
    public static void main(String[] args) {
        Student s1 = new Student("Jan", "Nowak", 1, "dst");
        Student s2 = new Student("Adam", "Kowal", 2, "db+");
        Student s3 = new Student("Jan", "Jan", 3, "bdb");
        Student s4 = new Student("Mariusz", "Bogdan", 4, "bdb");
        Student s5 = new Student("Alicja", "Polak", 5, "dst");
        Student s6 = new Student("Anna", "Alicka", 6, "db");
        Student s7 = new Student("Adam", "Małysz", 7, "bdb");
        TreeMap<Student,String> mapa = new TreeMap<>();
        mapa.put(s1,Student.getOcena(s1));
        mapa.put(s2,Student.getOcena(s2));
        mapa.put(s3,Student.getOcena(s3));
        mapa.put(s4,Student.getOcena(s4));
        mapa.put(s5,Student.getOcena(s5));
        mapa.put(s6,Student.getOcena(s6));
        mapa.put(s7,Student.getOcena(s7));
        Student.wypisz(mapa);
        Student.changeOcena(mapa, 3, "cel");
        System.out.println("\n");
        Student.wypisz(mapa);
    }

}

class Student implements Comparable<Student> {
    public Student(String im, String nazw, int id, String ocena){
        this.imie = im;
        this.nazwisko = nazw;
        this.id = id;
        this.ocena = ocena;
    }
    public int compareTo(Student other) {
        if (this.nazwisko.compareTo(other.nazwisko) == 0) {
            if (this.imie.compareTo(other.imie) == 0) {
                if (this.id < other.id) {
                    return -1;
                } else {
                    return 1;
                }
            }
            else {
                return this.imie.compareTo(other.imie);
            }
        }
        else{
            return this.nazwisko.compareTo(other.nazwisko);
        }
    }
    public static int getId(Student s){
        return s.id;
    }
    public static String getOcena(Student s){
        return s.ocena;
    }
    public static void zmOcena(Student o, String ocena){
        o.ocena = ocena;
    }

    public static String getImie(Student st) {
        return st.imie;
    }

    public static String getNazwisko(Student st) {
        return st.nazwisko;
    }
    public static void changeOcena(TreeMap<Student, String> map, int n, String value){
        for(Student o : map.keySet()){
            if(Student.getId(o)==n)
                zmOcena(o, value);
        }
    }
    public static void wypisz(TreeMap<Student,String> mapa){
        for (Student st:mapa.keySet()) {
            System.out.println(Student.getImie(st)+" "+Student.getNazwisko(st)+" "+Student.getId(st)+":"+Student.getOcena(st));
        }
    }
    public int id;
    public String imie;
    public String nazwisko;
    public String ocena;
}

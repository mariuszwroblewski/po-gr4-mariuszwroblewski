package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium13;

import java.util.*;

public class Zadanie2 implements Comparable<Zadanie2> {

    @Override
    public int compareTo(Zadanie2 other) {
        if (nazwisko.compareTo(other.nazwisko) < 0) {
            return -1;
        }
        else if (nazwisko.compareTo(other.nazwisko) > 0) {
            return 1;
        }
        return 0;
    }
    public void dodaj(String nazwisko, String ocena){
        this.nazwisko=nazwisko;
        this.ocena=ocena;
    }
    public String nazwisko;
    public String ocena;
    public static void main(String[] args) throws InterruptedException {
        Map<String,String> mapa=new TreeMap<>();
        Scanner input = new Scanner(System.in);
        while(true){
            System.out.println("Menu:\n1.dodaj-dodaj studenta\n2.usun-usuń usun studenta\n3.zmien-zmian ocene studetowi\n4.wypisz-wyswiatla studentow i oceny\n5.wyjdz- koniec programu");
            String zm= input.next();
            switch (zm){
                case "dodaj":
                    System.out.println("Podaj nazwisko: ");
                    String nazwisko = input.next();
                    System.out.println("Podaj ocene ");
                    String ocena = input.next();
                    mapa.put(nazwisko,ocena);
                    break;
                case "zmien":
                    System.out.println("Podaj nazwisko do zmiany oceny: ");
                    String nazwisko2 = input.next();
                    if(!mapa.containsKey(nazwisko2)){
                        System.out.println("Błędne nazwisko!");
                        break;
                    }
                    System.out.println("Podaj nowa ocene: ");
                    String ocena1 = input.next();
                    mapa.put(nazwisko2,ocena1);
                    break;
                case "usun":
                    System.out.println("Podaj nazwisko do usniecia: ");
                    String nazwisko1 = input.next();
                    mapa.remove(nazwisko1);
                    break;
                case "wypisz":
                    System.out.println("Studenci:("+mapa.size()+")");
                    for(Map.Entry<String,String> entry : mapa.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    System.out.println(key + ": " + value);
                    }
                    break;
                case "zakoncz":
                    System.out.println("Koniec");
                    System.exit(0);
                default:
                    System.out.println("Blędna komenda");
            }
        }
    }
}


package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium13;

import java.util.*;

public class Zadanie1 implements Comparable<Zadanie1>{
    public Zadanie1(String opis, int prior){
        this.Priorytet=prior;
        this.Opis=opis;
    }
    public void setOpis(String opis){
        this.Opis=opis;
    }
    public void setPriorytet(int prior){
        this.Priorytet=prior;
    }

    public int getPriorytet() {
        return Priorytet;
    }

    public String getOpis() {
        return Opis;
    }
    @Override
    public int compareTo(Zadanie1 o) {
        if(this.Priorytet>o.Priorytet){
            return 1;
        }
        else if(this.Priorytet<o.Priorytet){
            return -1;
        }
        return 0;
    }
    public String Opis;
    public int Priorytet;
    public static void main(String[] args){
        PriorityQueue<Zadanie1> ToDoList=new PriorityQueue<>();
        Scanner input = new Scanner(System.in);
        while(true){
            System.out.println("Obecna lista zadań:");
            for(Zadanie1 zad:ToDoList){
                System.out.println("Priorytet: "+zad.getPriorytet()+" Opis: "+zad.getOpis());
            }
            System.out.println("Menu:\n1.dodaj-dodaj nowe zadanie\n2.nastepne-usuń najpilniejsze zadanie\n3.zakoncz-wyjdz z programu");
            String zm= input.next();
            switch (zm){
                case "dodaj":
                    System.out.println("Podaj opis: ");
                    String opis = input.next();
                    System.out.println("Podaj priorytet ");
                    int prior = input.nextInt();
                    Zadanie1 task=new Zadanie1(opis,prior);
                    ToDoList.add(task);
                    break;
                case "nastepne":
                    ToDoList.remove();
                    break;
                case "zakoncz":
                    System.out.println("Koniec");
                    System.exit(0);
                default:
                    System.out.println("Blędna komenda");
            }
        }
    }
}

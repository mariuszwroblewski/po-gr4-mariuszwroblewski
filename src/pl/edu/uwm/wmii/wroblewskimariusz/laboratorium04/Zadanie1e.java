package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium04;

public class Zadanie1e {
    public static void main(String[] args){
        System.out.print("Napis nr 1: ");
        String str = "maniekni";
        System.out.println(str);
        System.out.print("Napis nr 2: ");
        String subStr ="ni";
        System.out.println(subStr);
        System.out.println("Indeksy wystąpień napisu 2 w napisie 1");
        int[] tab=where(str,subStr);
        for (int i=0;i<tab.length;i++){
            System.out.println(tab[i]);
        }
    }
    public static int[] where(String str, String subStr)
    {
        int dl=subStr.length();
        int[] wynik = new int[3];
        int n=0;
        for (int i = 0 ; i < str.length()-subStr.length()+1 ; i++)
        {
            if(str.substring( i,i+dl).equals(subStr)) {
                wynik[n]+=i;
                n++;
            }
        }
        return wynik;
    }

}


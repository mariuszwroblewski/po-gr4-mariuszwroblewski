package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium04;

public class Zadanie1b {
    public static void main(String[] args){
        System.out.print("Napis nr 1: ");
        String str = "maniek";
        System.out.println(str);
        System.out.print("Napis nr 2: ");
        String subStr ="ni";
        System.out.println(subStr);
        System.out.println("Ilośc wystąpień napisu 2 w napisie 1");
        System.out.println(countSubStr(str,subStr));
    }
    public static int countSubStr(String str, String subStr)
    {
        int dl=subStr.length();
        int wynik = 0;
        for (int i = 0 ; i < str.length()-subStr.length()+1 ; i++)
        {
                if(str.substring( i,i+dl).equals(subStr)) {
                    wynik++;
                }
        }
        return wynik;
    }

}

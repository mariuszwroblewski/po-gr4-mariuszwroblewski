package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium04;
import java.lang.StringBuffer;

public class Zadanie1h {
    public static void main(String[] args){
        System.out.print("Napis: ");
        String str = "1234567812337433232";
        System.out.println(str);
        System.out.println("Napis po modyfikacji");
        System.out.println(nice(str,2,":"));
    }
    public static String nice(String str,int coIle,String znak)
    {
        StringBuffer newstr= new StringBuffer();
        int n=0;
        for (int i = str.length()-1 ; i >=0; i--) {
            if((n>0)&&(n%coIle==0)){
                newstr.append(znak);
            }
            newstr.append(str.charAt(i));
            n++;

        }
        return newstr.reverse().toString();
    }

}


package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium04;
import java.util.Scanner;
public class Zadanie1c {
    public static void main(String[] args){
        Scanner input= new Scanner(System.in);
        System.out.println("Podaj napis:");
        String str = input.next();
        System.out.println(middle(str));
    }
    public static String middle(String str){
        String wynik="";
        int mid=(int)str.length()/2;
        if(str.length()%2==0){
            wynik=str.substring(mid-1,mid+1);
        }
        else{
            wynik=str.substring(mid,mid+1);
        }
        return wynik;
    }
}

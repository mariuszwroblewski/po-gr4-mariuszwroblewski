package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium04;

public class Zadanie1a {
    public static void main(String[] args){
    System.out.println(countChar("napsisss",'s'));
    }
    public static int countChar(String str, char c)
    {
        int wynik = 0;
        char znak;
        for (int i = 0 ; i < str.length() ; i++)
        {
            znak = str.charAt(i);
            if(znak == c)
            {
                wynik++;
            }
        }
        return wynik;
    }

}

package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium04;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Zadanie5
{
    public static void main(String[] args){
        BigDecimal kapital=new BigDecimal(args[0]);
        BigDecimal stopa=new BigDecimal(args[1]);
        String a=args[2];
        int n=Integer.parseInt(a);
        for(int i=0;i<n;i++){
            System.out.println("Kapital po "+i+" latach");
            kapital=kapital.add(kapital.multiply(stopa));
            kapital=kapital.setScale(2,RoundingMode.HALF_UP);
            System.out.println(kapital);
        }
    }
}


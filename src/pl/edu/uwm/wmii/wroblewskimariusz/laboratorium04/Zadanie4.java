package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium04;
import java.math.BigInteger;

public class Zadanie4 {
    public static void  main(String[] args){
        int n=Integer.parseInt(args[0]);
        BigInteger wynik=new BigInteger("0");
        BigInteger podst=new BigInteger("2");
        int i=0;
        while (i<n*n){
            wynik=wynik.add(podst.pow(i));
            i++;
        }
        System.out.println(wynik.toString());
    }
}

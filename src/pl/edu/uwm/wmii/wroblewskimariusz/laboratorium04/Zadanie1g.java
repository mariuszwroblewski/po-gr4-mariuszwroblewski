package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium04;
import java.lang.StringBuffer;

public class Zadanie1g {
    public static void main(String[] args){
        System.out.print("Napis: ");
        String str = "12345678123343332";
        System.out.println(str);
        System.out.println("Napis po modyfikacji");
        System.out.println(nice(str));
    }
    public static String nice(String str)
    {
        StringBuffer newstr= new StringBuffer();
        int n=0;
        for (int i = str.length()-1 ; i >=0; i--) {
            if((n>0)&&(n%3==0)){
                newstr.append("'");
            }
                newstr.append(str.charAt(i));
                n++;

        }
        return newstr.reverse().toString();
    }

}


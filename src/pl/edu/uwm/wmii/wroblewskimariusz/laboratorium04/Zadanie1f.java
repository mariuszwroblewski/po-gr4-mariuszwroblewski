package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium04;
import java.lang.StringBuffer;

public class Zadanie1f {
    public static void main(String[] args){
        System.out.print("Napis: ");
        String str = "nApIs";
        System.out.println(str);
        System.out.println("Napis po zamianie");
        System.out.println(change(str));
    }
    public static String change(String str)
    {
        StringBuffer newstr= new StringBuffer("");
        for (int i = 0 ; i < str.length(); i++) {
            if (Character.isUpperCase(str.charAt(i))) {
                newstr.append(Character.toLowerCase(str.charAt(i)));
            } else {
                newstr.append(Character.toUpperCase(str.charAt(i)));
            }
        }
        return newstr.toString();
    }

}


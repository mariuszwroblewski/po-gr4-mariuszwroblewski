package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium11;

public class PairUtil<T> extends Pair<T>{
    public static <T> PairUtil<T> swap(Pair<T> object){
        PairUtil<T> wynik=new PairUtil<>();
        wynik.setFirst(object.getFirst());
        wynik.setSecond(object.getSecond());
        return wynik;
    }
}

package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium11;
import java.time.LocalDate;

public class ArrayUtil<T extends Comparable<? super T>> {
    public static <T extends Comparable<T>> void swap(T a, T b){
        T temp=a;
        a=b;
        b=temp;
    }
    public static <T extends Comparable<T>> boolean isSorted(T[] a){
        for(int i=0; i<a.length; i++){
            if(a[i].compareTo(a[i+1]) == -1){
                return true;
            }
            else{
                break;
            }
            }
        return false;
    }
    public static <T extends Comparable<T>> int binSearch(T[] a,T obj){
        for (int i=0;i<a.length;i++){
            if(a[i].equals(obj)){
                return i;
            }
        }
        return -1;
    }
    public static <T extends Comparable<T>> void selectionSort(T[] a){
        for (int i=0;i<a.length;i++){
            int ind=i;
            for (int j=i+1;j<a.length;j++){
                if(a[ind].compareTo(a[j])==1){
                    ind=j;
                }
            }
            System.out.print("[");
            for(T k:a){
                System.out.print(k.toString()+" ");
            }
            System.out.print("]");
            T temp=a[ind];
            a[ind]=a[i];
            a[i]=temp;
            System.out.println("\n");
        }
    }

    public static <T extends Comparable<? super T>> void mergeSort(T[] tab) {
        if(tab == null) {
            return;
        }
        if(tab.length > 1) {
            int mid = tab.length / 2;
            T[] left = (T[]) new Comparable[mid];
            System.arraycopy(tab, 0, left, 0, mid);
            T[] right = (T[])new Comparable[tab.length - mid];
            System.arraycopy(tab, mid, right, 0, tab.length - mid);
            mergeSort(left);
            mergeSort(right);

            int i = 0;
            int j = 0;
            int k = 0;
            while(i < left.length && j < right.length) {
                if(left[i].compareTo(right[j]) < 0) {
                    tab[k] = left[i];
                    i++;
                }
                else {
                    tab[k] = right[j];
                    j++;
                }
                k++;
            }
            while(i < left.length) {
                tab[k] = left[i];
                i++;
                k++;
            }
            while(j < right.length) {
                tab[k] = right[j];
                j++;
                k++;
            }
        }
    }
    public static void main(String[] args){
        Integer[] tab1={64,25,12,22,11};
        for(Integer i:tab1){
            System.out.print(i.toString()+" ");
        }
        System.out.print("isSorted:"+ArrayUtil.isSorted(tab1)+" ");
        System.out.println("binSearch:"+ArrayUtil.binSearch(tab1,1)+" ");
        System.out.println("selectionSort");
        ArrayUtil.selectionSort(tab1);
        Integer[] tab3={3,4,5,2,3,234,555,1,2,4,4,1};
        System.out.println("mergeSort");
        for(Integer i:tab3){
            System.out.print(i+" ");
        }
        System.out.println("");
        ArrayUtil.mergeSort(tab3);
        for(Integer i:tab3){
            System.out.print(i+" ");
        }
        System.out.println("\n\n");
        System.out.println("mergeSort");
        LocalDate[] data={LocalDate.now().plusYears(1).plusMonths(2),LocalDate.now().minusYears(100).minusDays(22),LocalDate.now().plusYears(10),LocalDate.now().plusYears(2)};
        for(LocalDate i:data){
            System.out.print(i.toString()+" ");
        }
        System.out.println("");
        ArrayUtil.mergeSort(data);
        for(LocalDate i:data){
            System.out.print(i.toString()+" ");
        }
        System.out.println("");
        System.out.println("\n\n");
        Integer[] tab2={4,3,4,5};
        for(Integer i:tab2){
            System.out.print(i.toString()+" ");
        }
        for(Integer i:tab2){
            System.out.print(i.toString()+" ");
        }
        System.out.print("isSorted:"+ArrayUtil.isSorted(tab2)+" ");
        System.out.println("binSearch:"+ArrayUtil.binSearch(tab2,4)+" ");
        LocalDate[] data1={LocalDate.now().plusYears(1),LocalDate.now().minusYears(3),LocalDate.now().plusYears(10)};
        for(LocalDate i:data1){
            System.out.print(i.toString()+" ");
        }
        System.out.print("isSorted:"+ArrayUtil.isSorted(data1)+" ");
        System.out.println("binSearch:"+ArrayUtil.binSearch(data1,LocalDate.now()));
        System.out.println("selectionSort");
        ArrayUtil.selectionSort(data1);
        for(LocalDate i:data1){
            System.out.print(i.toString()+" ");
        }
        LocalDate[] data2={LocalDate.now(),LocalDate.now().plusMonths(2),LocalDate.now().plusYears(10)};
        for(LocalDate i:data2){
            System.out.print(i.toString()+" ");
        }
        System.out.print("isSorted:"+ArrayUtil.isSorted(data2)+" ");
        System.out.println("binSearch:"+ArrayUtil.binSearch(data2,LocalDate.now()));


    }
}

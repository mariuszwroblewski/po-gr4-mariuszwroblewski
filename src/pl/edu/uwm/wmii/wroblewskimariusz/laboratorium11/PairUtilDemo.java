package pl.edu.uwm.wmii.wroblewskimariusz.laboratorium11;

public class PairUtilDemo {
    public static void main(String[] args){
        String[] words = { "Ala", "ma", "psa", "i", "kota" };
        Pair<String> mm = ArrayAlg.minmax(words);
        System.out.println("min = " + mm.getFirst());
        System.out.println("max = " + mm.getSecond());
        PairUtil.swap(mm);
        System.out.println("min = " + mm.getFirst());
        System.out.println("max = " + mm.getSecond());
    }
}

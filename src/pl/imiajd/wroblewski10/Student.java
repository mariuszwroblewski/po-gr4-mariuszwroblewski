package pl.imiajd.wroblewski10;


public class Student extends Osoba implements Cloneable,Comparable<Osoba> {
    public int compareTo(Student other){
        if(super.compareTo(other)==0){
            if(sredniaOcen< other.sredniaOcen) {
                return -1;
            }
            else if(sredniaOcen> other.sredniaOcen){
                return 1;
            }
            else {
                return 0;
            }
        }
        return super.compareTo(other);
    }
    public void setSredniaOcen(double a){
        sredniaOcen=a;
    }
    public double getSredniaOcen(){
        return sredniaOcen;
    }
    private double sredniaOcen;
}

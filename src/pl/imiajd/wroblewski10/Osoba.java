package pl.imiajd.wroblewski10;

import java.time.LocalDate;

public class Osoba implements Cloneable,Comparable<Osoba>{
    @Override
    public boolean equals(Object otherObject){
        if(this==otherObject){
            return true;
        }
        if(otherObject==null){
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        Osoba other=(Osoba) otherObject;

        return nazwisko.equals(other.nazwisko)&&
                dataUrodzenia.equals(other.dataUrodzenia);

    }
    @Override
    public int compareTo(Osoba other)
    {
        if (nazwisko.compareTo(other.nazwisko) < 0) {
            return -1;
        }
        if (nazwisko.compareTo(other.nazwisko) > 0) {
            return 1;
        }
        else {
            if(dataUrodzenia.compareTo(other.dataUrodzenia)<0) {
                return -1;
            }
            else if(dataUrodzenia.compareTo(other.dataUrodzenia)>0){
                return 1;
            }
            return 0;
        }
    }
    @Override
    public String toString(){
        return getClass().getSimpleName()+"["+nazwisko+" , "+
                dataUrodzenia.getYear()+"-"+dataUrodzenia.getDayOfMonth()+
                "-"+dataUrodzenia.getDayOfMonth()+"]";
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }
    public String getNazwisko(){
        return nazwisko;
    }
    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }
    public void setDataUrodzenia(int rok,int miesiac, int dzien){
       dataUrodzenia=LocalDate.of(rok,miesiac,dzien);
    }
    private String nazwisko;
    public LocalDate dataUrodzenia;
}

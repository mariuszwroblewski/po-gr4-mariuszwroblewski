package pl.imiajd.wroblewski08;

import java.time.LocalDate;

public class Student extends Osoba {
    public Student(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek) {
        super(nazwisko,imiona,dataUrodzenia,plec);
        this.kierunek = kierunek;
    }
    @Override
    public String getOpis() {
        return "Imiona:"+super.getImiona()+
                " Urodzony:"+super.getdataUrodzenia()+
                " Plec:"+super.getPlec()+
                " Kierunek studiów:"+ this.getKierunek()+
                " Srednia ocen:"+this.getSredniaOcen();
    }
    public void setSredniaOcen(double sr){
        this.sredniaOcen=sr;
    }
    public double getSredniaOcen(){
        return sredniaOcen;
    }
    public String getKierunek(){
        return kierunek;
    }

    private String kierunek;
    double sredniaOcen;
}
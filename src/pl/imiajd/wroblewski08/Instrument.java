package pl.imiajd.wroblewski08;

import java.time.LocalDate;
public abstract class Instrument {
    public abstract void dzwiek();

    public String getProducent(){
        return producent;
    }

    public LocalDate getRokProdukcji(){
        return rokProdukcji;
    }
    @Override
    public boolean equals(Object otherObject){
        if(this==otherObject){
            return true;
        }
        if(otherObject==null){
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        Instrument other=(Instrument) otherObject;

        return producent.equals(other.producent)&&
                rokProdukcji.equals(other.rokProdukcji);

    }
    @Override
    public String toString(){
       return "Producent: "+producent+" Rok produkcji: "+rokProdukcji;
    }
    String producent;
    LocalDate rokProdukcji;
}
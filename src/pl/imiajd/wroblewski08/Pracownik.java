package pl.imiajd.wroblewski08;

import java.time.LocalDate;

public class Pracownik extends Osoba {
    public Pracownik(String nazwisko, String[] imiona, LocalDate dataUrodzenia, LocalDate dataZatrudnienia, boolean plec, double pobory) {
        super(nazwisko,imiona,dataUrodzenia,plec);
        this.pobory = pobory;
        this.dataZatrudnienia=dataZatrudnienia;
    }

    public double getPobory() {
        return pobory;
    }
    @Override
    public String getOpis() {
        return "Imiona:"+super.getImiona()+
                " Urodzony:"+super.getdataUrodzenia()+
                " Plec:"+super.getPlec()+
                " Pobory:"+this.getPobory()+ "zł "+
                " Zatrudniony:"+this.getDataZatrudnieia();
    }
    public LocalDate getDataZatrudnieia(){
        return dataZatrudnienia;
    }
    private double pobory;
    LocalDate dataZatrudnienia;
}
package pl.imiajd.wroblewski;

public class Nauczyciel extends Osoba {
    public Nauczyciel(String nazwisko, int rok, int pensja) {
        super(nazwisko, rok);
        this.pensja=pensja;
    }
    public int getPensja(){
        return pensja;
    }
    public void setPensja(int i){
        this.pensja=i;
    }
    public String getNazwisko(){
        return super.getNazwisko();
    }
    public int getRokUrodzenia(){
        return super.getRokUrodzenia();
    }
    @Override
    public String toString() {
        return super.toString()+"Pensja="+pensja;
    }
    private int pensja;
}

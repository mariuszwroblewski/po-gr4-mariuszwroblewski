package pl.imiajd.wroblewski;
import java.awt.*;

public class BetterRectangle extends Rectangle {
    public BetterRectangle(){
        super(5,5,10,10);
        //super.setLocation(5,5);
        //super.setSize(10,10);

    }
    public int getPerimeter(){
        return this.height*2+this.width*2;
    }
    public int getArea(){
        return this.height*this.width;
    }
}


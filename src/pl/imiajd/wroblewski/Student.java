package pl.imiajd.wroblewski;

import java.time.LocalDate;

public class Student extends Osoba {
    public Student(String nazwisko, int rok,String kierunek) {
        super(nazwisko, rok);
        this.kierunek=kierunek;
    }
    public String getNazwisko(){
        return super.getNazwisko();
    }
    public int getRokUrozenia(){
        return super.getRokUrodzenia();
    }
    public String getKierunek(){
        return kierunek;
    }
    public void setKierunek(String kierunek){
        this.kierunek=kierunek;
    }
    @Override
    public String toString() {
        return super.toString()+"Kierunek="+kierunek;
    }

    private String kierunek;


}

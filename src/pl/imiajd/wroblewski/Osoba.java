package pl.imiajd.wroblewski;

import java.time.LocalDate;
import java.util.Arrays;

public class Osoba {
    public Osoba(String nazwisko,int rok){
        this.nazwisko=nazwisko;
        this.rokUridzenia=rok;
    }
    public String getNazwisko(){
        return nazwisko;
    }
    public void setNazwisko(String nazwisko){
        this.nazwisko=nazwisko;
    }
    public int getRokUrodzenia(){
        return rokUridzenia;
    }
    public void setRokUrodzenia(int rok){
        this.rokUridzenia=rok;
    }
    @Override
    public String toString(){
        return "Nazwisko="+nazwisko+"  Rok urodzenia="+rokUridzenia+" ";
    }
    private String nazwisko;
    private int rokUridzenia;


}

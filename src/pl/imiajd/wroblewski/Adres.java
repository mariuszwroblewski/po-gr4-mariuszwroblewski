package pl.imiajd.wroblewski;

public class Adres {
    public Adres(String ulica, int nr_domu,int nr_miesz, String miasto, int kod){
        this.ulica=ulica;
        this.numer_domu=nr_domu;
        this.numer_mieszkania=nr_miesz;
        this.miasto=miasto;
        this.kod_pocztowy=kod;
    }
    public Adres(String ulica, int nr_domu,String miasto, int kod){
        this.ulica=ulica;
        this.numer_domu=nr_domu;
        this.miasto=miasto;
        this.kod_pocztowy=kod;
    }
    public void pokaz(){
        System.out.print("Kod pocztowy: "+kod_pocztowy+"  ");
        System.out.println("Miasto:  "+miasto);
        System.out.println("Ulica: "+ulica);
        System.out.println("Numer domu: "+numer_domu);
        System.out.println("Numer mieszkania: "+numer_mieszkania+"\n");
    }
    public boolean przed(Adres other){
        if(this.kod_pocztowy<other.kod_pocztowy){
            return true;
        }
        else {
            return false;
        }
    }
    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private int kod_pocztowy;
}
